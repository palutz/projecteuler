import scala.annotation.tailrec

def calculatePrime(n: Long) = {
  // (n: Int) => (2 to n) |> (r => r.foldLeft(r.toSet)((ps, x) => if (ps(x)) ps -- (x * x to n by x) else ps))
  (2 to n).{
    r => r.foldLeft(r.toSet)((ps, x) => if (ps(x)) ps -- (x * x to n by x) else ps)
  }
}


@tailrec
def calcPrime(counter: Int, upTo: Int, primeList: List[Int]): List[Int] = {
  def innerCalc(x: Int, lPrime: List[Int]): Option[Int] = {
    lPrime.flatMap(a => if(x % a == 0) Some(a) else None) match {
      case y :: ys => None // lPrime.toArray // returning a list of number that are factor of x, so x is not a prime number
      case _ => Some(x) // (lPrime :+ x).toArray  // no factors found, x is a prime number
    }
  }
  Console.println(s"counter=$counter upTo=$upTo")
  if(counter <= upTo) {
    innerCalc(counter, primeList) match {
      case Some(x) => {
        Console.println(s"$x")
        calcPrime(counter + 1, upTo, primeList :+ x)
      }
      case _ => calcPrime(counter + 1, upTo, primeList)
    }
  } else
    primeList
}

calcPrime(14, 1000000, List(2,3,5,7,11,13))

